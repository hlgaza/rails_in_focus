require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  test "should get world" do
    get welcome_world_url
    assert_response :success
  end

  test "should get numbers" do
    get welcome_numbers_url
    assert_response :success
  end

end
