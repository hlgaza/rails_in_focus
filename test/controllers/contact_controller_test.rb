require 'test_helper'

class ContactControllerTest < ActionDispatch::IntegrationTest
  test "should get me" do
    get contact_me_url
    assert_response :success
  end

end
